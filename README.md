# README #

This is a simple application, using a basic JSP and JSTL illustrating a simple webpage with cart functionality. This code is not an industry standard but it helps understanding how certain things work in JSP and JSTL. 
This cart implementation stores cart data in the database as opposed to in cookie. This saves storing large amounts of data in the client side as well as potentially helps track cart-related data on the server side (like abandoned carts for example). It seems it could also be more handy when handling product availability at later stages (but that's just a thought). 

## Important things included in this app are ##
* MVC structure
* Handling server side data with JSTL tags
* Handling cart in database and by using cookie with user ID 
* Connecting to database with JNDI DataSource object
* Firing SQL queries and calling stored procedures
* Generic error page

## How things are done? ##

* First, in order to identify a user I plant him a cookie with UUID which is actually a copied session ID of the first time he/she logged in. Once user adds any product to cart I use this UUID to identify the cart which I store in the database. 
* At repeated entry I read the UUID from cookie and search for cart contents in the database, if found I read them and save in session to present to user.
* Each time a cart-related database operation is peformed I create a CartObj - a simplified representation of cart (mainly reflecting the quantity of products) on the client side and save it in session. 
* Using JSTL tags I read cart contents for specific UUID from the database. This happens in view_cart.jsp. 
* The important cart, database related operations are done using stored prodcedures which provides a little more security and guarantees clean data (transaction) - these are add to cart, buy cart and clean cart operations. 

### What is this repository for? ###

This code comes with no guarantee whatsoever and should not be used for commercial purposes. It's here only for learning purposes, feel free to use the code snippets included here to develop your own applications, and if you find any better way of doing things, it would be perfect if you could let me know. 

### How do I get set up? ###

In order to set this project up, the only thing required should be to fork the repo and make sure you have a MySQL connector .jar file included in the build path as well as your Tomcat's lib folder. You may need to modify your Tomcat's context.xml to incorporate the data source and along with this you may need to modify your web.xml. 

Entry in my context.xml is the following: 
```
<Resource name="jdbc/cart" auth="Container" type="javax.sql.DataSource"
               maxTotal="100" maxIdle="30" maxWaitMillis="10000"
               username="your_username" password="your_password" driverClassName="com.mysql.jdbc.Driver"
               url="jdbc:mysql://localhost:3306/your_datbase_name"/>
```
Corresponding entry in web.xml : 
```
<resource-ref>
      <description>DB Connection</description>
      <res-ref-name>jdbc/cart</res-ref-name>
      <res-type>javax.sql.DataSource</res-type>
      <res-auth>Container</res-auth>
  </resource-ref>
```

SQL scripts to create a database, tables and example data are included.

### Testing ###

The .test package contains unit tests that I used to verify required java class functionality. For JDBCUtil testing I used a separate test database which was a copy of the original database, so that "production" data is not harmed. The connection to test database was retrieved in setUpBeforeClass() function and used for all the unit tests. 
I found this way of handling database tests quite convenient, but if you have any other approach to this, it would be great if you could let me know, always happy to learn :) 

### Who do I talk to? ###

* Repo owner or admin
