<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8" isErrorPage="true"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"
     prefix="c" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Ooops !</title>
</head>
<body>
<h1>Ooops !</h1>
<div class="container">
<h3>Something went wrong...</h3>
<p>${pageContext.errorData.throwable.cause}</p>
<a href='<c:url value="/Cart"/>'>Back to homepage</a>
</div>
</body>
</html>