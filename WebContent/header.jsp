<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="css/style.css">
<title>Products</title>
</head>
<body>
	<div class="container">
		<h1>Adam's Shop</h1>
		<div class="main">
			<div class="controls">
				<c:choose>
					<c:when test="${sessionScope.user != null}">
						<div class="welcome">
							<span>Hello ${sessionScope.user.email} </span> <a href='<c:url value="/Cart?action=logout"/>'>Logout</a>
						</div>
					</c:when>
					<c:otherwise>
						<div class="welcome">
							<a href='<c:url value="/Cart?action=login"/>'>Login</a>
						</div>
					</c:otherwise>
				</c:choose>
				<div class="cartContentsContainer">
				<c:choose>
					<c:when test="${sessionScope.cart != null && sessionScope.cart.count > 0}">
						<span>You have <a href='<c:url value="/Cart?action=viewCart"/>'>${sessionScope.cart.count} items</a> in your
							cart</span>
					</c:when>
					<c:otherwise>
						<span>Your cart is empty</span>
					</c:otherwise>
				</c:choose>
				</div>
			</div>
			<div class="clear"></div>