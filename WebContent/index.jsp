<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>

<!--  Header File !!  -->
<c:import url="header.jsp"></c:import>
<!-- Header File !! -->

<sql:setDataSource dataSource="jdbc/cart" var="ds" />
<sql:query var="results" dataSource="${ds}" sql="SELECT * FROM  products"></sql:query>
<table class="productsTable">
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Price</th>
        <th>Action</th>
    </tr>
	<c:forEach var="product" items="${results.rows}">
	<form action='<c:url value="/Cart"/>' method="post">
	   <input type="hidden" name="productId" value="${product.id}"/>
	   <input type="hidden" name="formAction" value="addToCart"/>
		<tr>
		   <td>${product.id}</td>
		   <td>${product.name}</td>
		   <td>${product.price}</td>
		   <td><input type="submit" name="submit" value="Add To Cart"/></td>
	    </tr>
    </form>
	</c:forEach>
</table>
</div> <!--  /main div -->
</div> <!--  /container div -->
</body>
</html>