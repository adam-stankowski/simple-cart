<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet"> 
<link rel="stylesheet" type="text/css" href="css/style.css">

<title>Login to Adam's Shop</title>
</head>
<body>

<div class="login">
	<form action='<c:url value="/Cart"/>' method="post">
	<input type="hidden" name="formAction" value="login" />
	<div class="formItem">
	   <label for="email">Email</label>
	   <input type="text" name="email" value="${sessionScope.email}"/>
	</div>
	<div class="formItem">
	   <label for="password">Password</label>
       <input type="password" name="password"/>
    </div>
    <div class="formItem">
       <input type="submit" name="submit" value="Login"/>
    </div>
    </form>
    <c:if test="${sessionScope.loginMessage != null}">
        <p class="error_message">${sessionScope.loginMessage}</p>
    </c:if>
</div>
</body>
</html>