<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>

<!--  Header File !!  -->
<c:import url="header.jsp"></c:import>
<!--  Header File !!  -->

<sql:setDataSource dataSource="jdbc/cart" var="ds" />
<sql:query var="results" dataSource="${ds}"
	sql="SELECT * FROM v_products_in_cart WHERE uuid=?">
	<sql:param value="${sessionScope.uuid}"></sql:param>
</sql:query>
<c:set var="cartTotal" value="${0}"></c:set>
<div class="cartContents">
	<div class="cartContentsHeader">
		<h3>Your cart contents:</h3>
		<a href='<c:url value="/Cart" />'>Back to homepage</a>
	</div>
	<table class="cartContentsTable">
		<tr>
			<th>Product name</th>
			<th>Price</th>
			<th>Quantity</th>
			<th>Total</th>
		</tr>
		<c:forEach var="item" items="${results.rows}">
			<tr>
				<td>${item.name}</td>
				<td>${item.price}</td>
				<td>${item.quantity}</td>
				<td>${item.quantity * item.price}</td>
			</tr>
			<c:set var="cartTotal"
				value="${cartTotal+ item.quantity * item.price}"></c:set>
		</c:forEach>
		<tr>
			<td colspan="4" class="cartContentsSummary">Cart total ${cartTotal}$</td>
		</tr>
	</table>
	<form class="buttonsForm" action='<c:url value="/Cart"/>' method="post">
		<input type="hidden" name="formAction" value="cartActions" /> <input
			type="submit" name="clearCart" value="Clear Cart" /> <input
			type="submit" name="buy" value="Buy" />
	</form>

</div>
</div>
<!--  /main div -->
</div>
<!--  /container div -->
</body>
</html>