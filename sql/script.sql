CREATE DATABASE shopping_cart;
CREATE DATABASE shopping_cart_test;

CREATE USER 'cart'@'localhost' IDENTIFIED BY 'cartPassword';
GRANT SELECT,INSERT,UPDATE,EXECUTE ON shopping_cart.* TO cart@localhost; 
GRANT SELECT ON mysql.proc to cart@localhost;

CREATE USER 'cart_test'@'localhost' IDENTIFIED BY 'cartTestPassword';
GRANT SELECT,INSERT,UPDATE,EXECUTE,DELETE ON shopping_cart_test.* TO cart_test@localhost;
GRANT SELECT ON mysql.proc to cart_test@localhost;

USE shopping_cart_test;
CREATE TABLE users(
id int not null primary key auto_increment,
email varchar(20) not null,
password varchar(20) not null,
unique (email));

CREATE TABLE products(
id int not null primary key auto_increment,
name nvarchar(20) not null,
price decimal(6,2)
);

CREATE TABLE cart(
id int not null primary key auto_increment,
user_id int not null,
foreign key (user_id) references users(id)
);

CREATE TABLE cart_product(
id int not null primary key auto_increment,
cart_id int not null,
product_id int not null, 
quantity int not null, 
foreign key (cart_id) references cart(id),
foreign key(product_id) references products(id)
); 

CREATE TABLE orders(
id int not null primary key auto_increment,
user_id int not null,
order_total decimal (6,2) not null,
foreign key(user_id) references users(id)
);

CREATE TABLE orders_products(
id int not null primary key auto_increment,
order_id int not null,
product_id int not null,
quantity int not null
);

INSERT INTO users (email, password) VALUES ('adam@wp.pl','password');
INSERT INTO products (name,price) VALUES ('Book', 39.99);
INSERT INTO products (name,price) VALUES ('Microwave', 299.99);
INSERT INTO products (name,price) VALUES ('Shoes', 199.00);
INSERT INTO cart (user_id) VALUES (1);
INSERT INTO cart_product (cart_id,product_id,quantity) VALUES (1,1,1);
INSERT INTO cart_product (cart_id,product_id,quantity) VALUES (1,2,2);
INSERT INTO cart_product (cart_id,product_id,quantity) VALUES (1,3,3);

ALTER TABLE cart MODIFY COLUMN user_id INT NULL;
ALTER TABLE cart ADD COLUMN uuid VARCHAR(40) NOT NULL;
ALTER TABLE cart ADD COLUMN created DATETIME NOT NULL;
ALTER TABLE cart_product DROP FOREIGN KEY cart_product_ibfk_1;
ALTER TABLE cart_product ADD CONSTRAINT 
cart_product_fk_cart_id FOREIGN KEY (cart_id) REFERENCES cart(id) ON DELETE CASCADE;


ALTER TABLE cart DROP FOREIGN KEY cart_ibfk_1;
ALTER TABLE cart DROP COLUMN user_id;


USE `shopping_cart_test`;
CREATE 
     OR REPLACE ALGORITHM = UNDEFINED 
    DEFINER = `root`@`localhost` 
    SQL SECURITY DEFINER
VIEW `shopping_cart_test`.`v_products_in_cart` AS
    select 
        `c`.`id` AS `cart_id`,
        `c`.`uuid` AS `uuid`,        
        `p`.`name` AS `name`,
        `p`.`price` AS `price`,
        `cp`.`quantity` AS `quantity`
    from
        ((`shopping_cart_test`.`cart` `c`
        join `shopping_cart_test`.`cart_product` `cp` ON ((`c`.`id` = `cp`.`cart_id`)))
        join `shopping_cart_test`.`products` `p` ON ((`p`.`id` = `cp`.`product_id`)));



CREATE OR REPLACE VIEW 'v_products_in_orders' AS
SELECT o.id as order_id, p.id as product_id, p.name as product_name,p.price, op.quantity FROM orders o 
inner join orders_products op on o.id = op.order_id
inner join products p on op.product_id = p.id;

ALTER TABLE products ADD COLUMN available_units int not null default 0;
ALTER TABLE orders DROP COLUMN order_total;


-- --------------------------------------------------------------------------------
-- Routine DDL
-- Note: comments before and after the routine body will not be stored by the server
-- --------------------------------------------------------------------------------
USE `shopping_cart_test`;
DROP procedure IF EXISTS `buy_cart`;

DELIMITER $$
USE `shopping_cart_test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buy_cart`(IN a_uuid VARCHAR(40), IN a_user_id INT)
BEGIN
DECLARE p_cart_id INT;
DECLARE p_order_id BIGINT;
DECLARE c_finished INT DEFAULT 0;
DECLARE c_prod_id INT;
DECLARE c_quan INT;
DECLARE cp_cursor CURSOR FOR 
SELECT cp.product_id, cp.quantity FROM cart_product cp 
INNER JOIN cart c ON c.id = cp.cart_id WHERE c.uuid = a_uuid;

DECLARE CONTINUE HANDLER FOR NOT FOUND SET c_finished=1;

START TRANSACTION;
	SELECT id INTO p_cart_id FROM cart WHERE uuid = a_uuid;	
	INSERT INTO orders (user_id) VALUES (a_user_id);
	SET p_order_id = last_insert_id();
	OPEN cp_cursor;

		get_products: LOOP
			FETCH CP_CURSOR INTO c_prod_id,c_quan;

			IF c_finished = 1 THEN
				LEAVE get_products;
			END IF;
			
			INSERT INTO orders_products (order_id,product_id,quantity) VALUES
				(p_order_id,c_prod_id,c_quan);
		
		END LOOP get_products;
	CLOSE cp_cursor;

	DELETE FROM cart_product WHERE cart_id = p_cart_id;
	DELETE FROM cart WHERE id = p_cart_id;

COMMIT;
END$$

DELIMITER ;






USE `shopping_cart_test`;
DROP procedure IF EXISTS `add_to_cart`;

DELIMITER $$
USE `shopping_cart_test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `add_to_cart`(IN a_uuid VARCHAR(40), IN productID INT)
BEGIN
DECLARE a_cart_id INT;
DECLARE a_cp_id INT;
DECLARE a_quantity INT;
START TRANSACTION;
SELECT id INTO a_cart_id FROM cart WHERE uuid = a_uuid;
IF a_cart_id IS NULL THEN
	INSERT INTO cart (uuid,created) values (a_uuid,NOW());
	SET a_cart_id = last_insert_id();
END IF;
SELECT id, quantity INTO a_cp_id, a_quantity FROM cart_product WHERE cart_id = a_cart_id AND product_id = productID;
IF a_cp_id IS NOT NULL THEN
	UPDATE cart_product SET quantity = a_quantity + 1 WHERE id = a_cp_id;
ELSE
	INSERT INTO cart_product (cart_id, product_id, quantity) values (a_cart_id,productID,1);
END IF;
COMMIT;
SELECT * FROM v_products_in_cart WHERE cart_id = a_cart_id;
END$$

DELIMITER ;



USE `shopping_cart_test`;
DROP procedure IF EXISTS `clear_cart`;

DELIMITER $$
USE `shopping_cart_test`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `clear_cart`(IN a_uuid VARCHAR(40))
BEGIN
DECLARE a_cart_id INT;
START TRANSACTION;
SELECT id into a_cart_id FROM cart WHERE uuid = a_uuid;
DELETE FROM cart_product WHERE cart_id = a_cart_id;
COMMIT;
SELECT * FROM v_products_in_cart WHERE cart_id = a_cart_id;
END$$

DELIMITER ;

DELIMITER ;

call add_to_cart('a1',2);

select * fradd_to_cartom cart;
select * from cart_product;

select * from mysql.proc;

select * from v_products_in_cart;

show create table cart_product;

DELETE FROM cart where uuid = 'a3';

select * from cart;
select * from users;

call clear_cart('a1');

select * from cart_product;
select * from cart;

call buy_cart('565D7D04D1191A25975F915E9832ECB3');

SELECT cp.product_id, cp.quantity FROM cart_product cp 
INNER JOIN cart c ON c.id = cp.cart_id WHERE c.uuid = 'C7B8BAB1ECCBD48B9FDA0AA9653F8D91';

select id from cart where uuid = 'C7B8BAB1ECCBD48B9FDA0AA9653F8D91';

SELECT user_id FROM cart WHERE id =26;
select * from orders_products;
SELECT * FROM orders;
select * from orders_products;

describe cart;



