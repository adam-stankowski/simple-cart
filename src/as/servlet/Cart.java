package as.servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import as.servlet.bean.CartObj;
import as.servlet.bean.User;
import as.servlet.util.JDBCUtil;

/**
 * Servlet implementation class Cart
 */
public class Cart extends HttpServlet {
  private static final long serialVersionUID = 1L;
  private DataSource ds;

  /**
   * @see HttpServlet#HttpServlet()
   */
  public Cart() {
    super();
    // TODO Auto-generated constructor stub
  }

  /**
   * @see Servlet#init(ServletConfig)
   */
  public void init(ServletConfig config) throws ServletException {

    try {
      // Getting the datasource object used for database connectivity
      InitialContext ctx = new InitialContext();
      Context env = (Context) ctx.lookup("java:comp/env");
      ds = (DataSource) env.lookup("jdbc/cart");
    } catch (NamingException e) {
      e.printStackTrace();
    }

  }

  /**
   * The get handler serves as a dispatcher and serves relevant pages based on
   * request url action parameter.
   * 
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    String uuid = identifyUsersUUID(request, response);
    fetchAndUpdateCartInSession(request, uuid);

    String action = request.getParameter("action");

    if (action == null) {
      request.getRequestDispatcher("/index.jsp").forward(request, response);
    } else if (action.equals("login")) {
      request.getRequestDispatcher("/login.jsp").forward(request, response);
    } else if (action.equals("viewCart")) {
      request.getRequestDispatcher("/view_cart.jsp").forward(request, response);
    } else if (action.equals("logout")) {
      logUserOut(request, response);
      request.getRequestDispatcher("/index.jsp").forward(request, response);
    }
  }

  /**
   * Post handler handles various actions related to button clicks on a site. It
   * handles login form, add to cart, clean cart and buy actions.
   * 
   * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
   *      response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {

    // Protects from loosing uuid in expiring session if site is left for long time
    // untouched
    String uuid = identifyUsersUUID(request, response);
    fetchAndUpdateCartInSession(request, uuid);

    String action = request.getParameter("formAction");

    if (action == null) {
      request.getRequestDispatcher("/index.jsp").forward(request, response);
    } else if (action.equals("login")) {

      if (logUserIn(request, response)) {
        request.getRequestDispatcher("/index.jsp").forward(request, response);
      } else {
        request.getRequestDispatcher("/login.jsp").forward(request, response);
      }
    } else if (action.equals("addToCart")) {

      addToCart(request, response);
    } else if (action.equals("cartActions")) {

      handleCartActions(request, response);
    }
  }

  /**
   * Verifies if unique user's Id id stored in session, if not it searches for it in
   * cookie, if not found the function creates the ID and saves it in both session
   * and cookie
   * 
   * @param request
   * @param response
   * @return UUID assigned to user
   */
  private String identifyUsersUUID(HttpServletRequest request,
      HttpServletResponse response) {

    String uuid = (String) request.getSession().getAttribute("uuid");
    if (uuid == null) {
      Cookie cookie = findCookie(request.getCookies(), "uuid");
      if (cookie == null) {
        uuid = request.getSession().getId();
        cookie = new Cookie("uuid", uuid);
        cookie.setMaxAge(5000);
        response.addCookie(cookie);
      } else {
        uuid = cookie.getValue();
      }
      request.getSession().setAttribute("uuid", uuid);
    }
    return uuid;
  }

  /**
   * Verifies if a user identified by email and password passed in request exists in
   * the database.
   * 
   * @param request
   * @param response
   * @return True iif user exists in the database, otherwise false
   */
  private boolean logUserIn(HttpServletRequest request,
      HttpServletResponse response) {
    String email = request.getParameter("email");
    String password = request.getParameter("password");
    boolean result = false;

    try (Connection con = ds.getConnection()) {
      User user = JDBCUtil.userExists(email, password, con);
      if (user != null) {
        request.getSession().setAttribute("user", user);
        result = true;
      } else {
        setLoginFailedAttributes(request, email, "Invalid username or password");
      }

    } catch (SQLException e) {
      setLoginFailedAttributes(request, email, "Invalid username or password");
      e.printStackTrace();
    } catch (IllegalArgumentException e) {
      setLoginFailedAttributes(request, email, e.getMessage());
    }

    return result;
  }

  /**
   * This function sets email and loginMessage session attributes after unsuccessful
   * login.
   * 
   * @param request
   *          Request object
   * @param email
   *          email used while logging in
   * @param message
   *          Message to be displayed to user on login retry
   */
  private void setLoginFailedAttributes(HttpServletRequest request, String email,
      String message) {
    request.getSession().setAttribute("email", email);
    request.getSession().setAttribute("loginMessage", message);
  }

  /**
   * Finds a Cookie with cookiename in an array of cookies
   * 
   * @param cookies
   *          Array of cookies to search
   * @param cookieName
   *          name of cookie to be found
   * @return found cookie, otherwise null
   */
  private Cookie findCookie(Cookie[] cookies, String cookieName) {
    if (cookies == null)
      return null;
    for (Cookie i : cookies) {
      if (i.getName().equals(cookieName)) {
        return i;
      }
    }
    return null;
  }

  /**
   * Adds a product passed in request parameter product id to cart for user
   * identified by uuid. Cart for user identified by uuid is saved in session.
   * 
   * @param request
   * @param response
   * @throws ServletException
   *           is thrown in case of issue and should be handled by a generic error
   *           page
   */
  private void addToCart(HttpServletRequest request, HttpServletResponse response)
      throws ServletException {

    int productId = Integer.parseInt(request.getParameter("productId"));
    String uuid = (String) request.getSession().getAttribute("uuid");
    try {
      CartObj cart = JDBCUtil.addToCart(uuid, productId, ds.getConnection());
      request.getSession().setAttribute("cart", cart);
      request.getRequestDispatcher("/index.jsp").forward(request, response);
    } catch (Exception e) {
      throw new ServletException(e);
      // TODO prepare generic error page
    }
  }

  /**
   * Retrieves a cart from database for particular uuid and stores its object in
   * session
   * 
   * @param request
   * @param uuid
   */
  private void fetchAndUpdateCartInSession(HttpServletRequest request, String uuid) {
    try {
      CartObj cart = JDBCUtil.fetchCart(uuid, ds.getConnection());
      request.getSession().setAttribute("cart", cart);
    } catch (SQLException e) {
      e.printStackTrace();
      // deliberately do nothing if cart was not properly read. Small harm to user.
    }
  }

  /**
   * Clears cart identified by particular uuid, passed in request attribute, in both
   * database and in session
   * 
   * @param request
   * @param response
   */
  private void clearCart(HttpServletRequest request, HttpServletResponse response) {
    String uuid = (String) request.getSession().getAttribute("uuid");

    try {
      CartObj cart = (CartObj) request.getSession().getAttribute("cart");
      cart = JDBCUtil.clearCart(uuid, cart, ds.getConnection());
      request.getSession().setAttribute("cart", cart);
      request.getRequestDispatcher("/index.jsp").forward(request, response);
    } catch (SQLException e) {
      e.printStackTrace();
    } catch (ServletException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Purchases the cart identified by particular uuid for particular user. at this
   * stage user needs to be logged in which is forced in doPost before this function
   * is triggered
   * 
   * @param request
   * @param response
   * @throws ServletException
   */
  private void buyCart(HttpServletRequest request, HttpServletResponse response)
      throws ServletException {
    String uuid = (String) request.getSession().getAttribute("uuid");
    User user = (User) request.getSession().getAttribute("user");
    try {
      CartObj cart = (CartObj) request.getSession().getAttribute("cart");
      cart = JDBCUtil.buyCart(uuid, user, cart, ds.getConnection());
      request.getSession().setAttribute("cart", cart);
      request.getRequestDispatcher("/thankyou.jsp").forward(request, response);
    } catch (SQLException e) {
      e.printStackTrace();
      throw new ServletException("Could not perform the sales process");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Function dispatches actions related to cart. The actions are : clear cart and
   * buy
   * 
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void handleCartActions(HttpServletRequest request,
      HttpServletResponse response) throws ServletException, IOException {

    if (request.getParameter("clearCart") != null) {
      clearCart(request, response);
    } else if (request.getParameter("buy") != null) {
      User user = (User) request.getSession().getAttribute("user");
      if (user == null) { // user needs to be logged in to buy stuff
        request.getRequestDispatcher("/login.jsp").forward(request, response);
      } else {
        buyCart(request, response);
      }
    }
  }

  /**
   * Logs user out by removing user object from session, removing uuid and uuid
   * cookie.
   * 
   * @param request
   * @param response
   */
  private void logUserOut(HttpServletRequest request, HttpServletResponse response) {
    request.getSession().removeAttribute("user");
    request.getSession().removeAttribute("uuid");
    Cookie cookie = findCookie(request.getCookies(), "uuid");
    if (cookie != null) {
      cookie.setMaxAge(0);
      response.addCookie(cookie);
    }
  }
}
