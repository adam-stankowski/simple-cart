package as.servlet.bean;

/**
 * Immutable class to represent single item in user's cart. These items are not
 * updated on server but synchronized and recreated from database, that's why this
 * object is immutable.
 * 
 * @author Adam Stankowski
 *
 */
public final class CartItem {
  private final String name;
  private final int quantity;

  public CartItem(String name, int quantity) {
    super();
    this.name = name;
    this.quantity = quantity;
  }

  public int getQuantity() {
    return quantity;
  }

  public String getName() {
    return name;
  }

}
