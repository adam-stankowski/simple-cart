package as.servlet.bean;

import java.util.ArrayList;

/**
 * This class is used to represent cart on the server side. CartObj stored in session
 * is re-created each time some cart manipulation occurs
 * 
 * @author Adam Stankowski
 *
 */
public final class CartObj {
  private final ArrayList<CartItem> items;
  private int count;

  public CartObj() {
    items = new ArrayList<>();
    count = 0;
  }

  public void addToCart(CartItem item) {
    items.add(item);
    count += item.getQuantity();
  }

  public Iterable<CartItem> getItems() {
    return new ArrayList<CartItem>(items);
  }

  public int getCount() {
    return count;
  }

}
