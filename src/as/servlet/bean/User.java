package as.servlet.bean;

/**
 * Class to represent user in the system. It constructs a user and checks
 * for correctness of email and password. If not correct, it throws an
 * IllegalArgumentException out of its constructor. Client program needs to handle
 * this exception. This is more powerful than providing a static method that would
 * verify the credentials, cause this way implementer would need to remember
 * to call the method each time before creating a user object. 
 * 
 * @author Adam Stankowski
 *
 */
public final class User {
  private final String email;
  private final String password;
  private int id;

  public User(String email, String password) {
    if (!validateEmail(email)) {
      throw new IllegalArgumentException(
          "Username does not meet the required criteria");
    }
    if (!validatePassword(password)) {
      throw new IllegalArgumentException(
          "Password does not meet the required criteria");
    }

    this.email = email;
    this.password = password;
  }

  /**
   * Retrieves username
   * 
   * @return
   */
  public String getEmail() {
    return email;
  }

  /**
   * Retrieves password
   * 
   * @return
   */
  public String getPassword() {
    return password;
  }

  /**
   * Function called to validate email correctness based on simple rules - exercise
   * only. The rules are following: a non space word before and after @, a dot and a
   * non space word.
   * 
   * @param aEmail
   *          - email address to be validated
   * @return
   */
  private boolean validateEmail(String aEmail) {
    String emailRegex = "\\w+[@]\\w+[.]\\w+";
    return aEmail.matches(emailRegex);
  }

  /**
   * Validates correctness of password based on simple rules - exercise only. The
   * rules are following: a Word without a space at least 6 characters. small,
   * capital or numbers
   * 
   * @param aPassword
   *          - a password to be validated for correctness
   * @return
   */
  private boolean validatePassword(String aPassword) {
    String passwordRegex = "\\w{6,}+";
    return aPassword.matches(passwordRegex);
  }
  
  /**
   * Retrieves user ID. This Id is associated with database ID
   * @return
   */
  public int getId() {
    return id;
  }
  
  /**
   * Enables setting of user ID
   * @param id
   */
  public void setId(int id) {
    this.id = id;
  }

}
