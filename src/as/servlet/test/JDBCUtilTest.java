package as.servlet.test;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import as.servlet.bean.CartItem;
import as.servlet.bean.CartObj;
import as.servlet.bean.User;
import as.servlet.util.JDBCUtil;

public class JDBCUtilTest {
  private static Connection con;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    con = DriverManager.getConnection(
        "jdbc:mysql://localhost:3306/shopping_cart_test", "cart_test",
        "cartTestPassword");
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    con.close();
  }

  @Test
  public void testValidUserExists() throws IllegalArgumentException, SQLException {
    assertNotNull(JDBCUtil.userExists("adam@wp.pl", "password", con));
  }

  @Test
  public void testNonExistentUser() throws IllegalArgumentException, SQLException {
    assertNull(JDBCUtil.userExists("ewa@wp.pl", "password", con));
  }

  @Test
  public void testExistingUserWrongPassword()
      throws IllegalArgumentException, SQLException {
    assertNull(JDBCUtil.userExists("adam@wp.pl", "wrongPassword", con));
  }

  @Test
  public void testAddToCart() throws SQLException {
    String uuid = "test1";
    CartObj actual = JDBCUtil.addToCart(uuid, 1, con);
    assertEquals(1, actual.getCount());

    for (CartItem item : actual.getItems()) {
      assertEquals("Book", item.getName());
      assertEquals(1, item.getQuantity());
    }

    deleteTestCart(uuid);
  }

  @Test
  public void testClearCart() throws SQLException {
    String uuid = "test1";
    CartObj actual = JDBCUtil.addToCart(uuid, 1, con);
    JDBCUtil.clearCart(uuid, actual, con);

    String sql = "SELECT * FROM v_products_in_cart WHERE uuid=?";
    try (PreparedStatement ps = con.prepareStatement(sql)) {
      ps.setString(1, uuid);
      ResultSet rs = ps.executeQuery();
      assertFalse(rs.first()); // there should be no rows
    }

    deleteTestCart(uuid);
  }

  private void deleteTestCart(String uuid) throws SQLException {
    String sql = "DELETE FROM cart WHERE uuid=?";
    try (PreparedStatement ps = con.prepareStatement(sql)) {
      ps.setString(1, uuid);
      ps.executeUpdate();
    }
  }

}
