package as.servlet.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import as.servlet.bean.CartItem;
import as.servlet.bean.CartObj;
import as.servlet.bean.User;

/**
 * JDBCUtil class containing static methods to communicate with database. This
 * function is not designed for inheritance hence all methods are final.
 * 
 * @author Adam Stankowski
 *
 */
public class JDBCUtil {

  /**
   * This function calls a database and checks if a user with given username and
   * password exists in the database, if yes it returns that user.
   * 
   * @param user
   *          user to be verified in the database
   * @param con
   *          Connection to database
   * @return User object which represents logged in user in the database.
   * @throws SQLException
   */
  public static final User userExists(String username, String password,
      Connection con) throws SQLException, IllegalArgumentException {

    User user = new User(username, password); // incorrect credentials throw exc
    String sql = "SELECT id FROM users WHERE email=? AND password=?";
    try (PreparedStatement ps = con.prepareStatement(sql)) {
      ps.setString(1, user.getEmail());
      ps.setString(2, user.getPassword());
      ResultSet rs = ps.executeQuery();

      if (!rs.first()) { // There is no row in DB
        return null;
      } else {
        user.setId(rs.getInt(1));
        return user;
      }
    }
  }

  /**
   * Function adds a product with id = productID to cart of a user identified by uuid
   * session id. Creating new cart each time is cheap so for sake of consistency with
   * DB I decide to re-create it each time a product is added
   * 
   * @param uuid
   *          User's unique session id assigned at each entry
   * @param productId
   *          Id of a product we want to add to cart
   * @param connection
   *          SQL connection object
   * @return CartObj representing cart contents of user represented by uuid
   * @throws SQLException
   */
  public static final CartObj addToCart(String uuid, int productId,
      Connection connection) throws SQLException {
    CartObj cart = new CartObj();
    String sql = "{CALL add_to_cart(?,?)}";
    try (CallableStatement cs = connection.prepareCall(sql)) {
      cs.setString(1, uuid);
      cs.setInt(2, productId);
      cart = resultSetToCartObj(cs);
    }
    return cart;
  }

  /**
   * Retrieves a cart for particular UUID from the database and returns Cart contents
   * as CartObj
   * 
   * @param uuid
   * @param con
   * @return
   * @throws SQLException
   */
  public static final CartObj fetchCart(String uuid, Connection connection)
      throws SQLException {
    String sql = "SELECT * FROM v_products_in_cart WHERE uuid=?";
    CartObj cart = new CartObj();
    try (PreparedStatement ps = connection.prepareStatement(sql)) {
      ps.setString(1, uuid);
      cart = resultSetToCartObj(ps);
    }
    return cart;
  }

  private static CartObj resultSetToCartObj(PreparedStatement ps)
      throws SQLException {
    CartObj cart = new CartObj();
    ResultSet rs = ps.executeQuery();
    while (rs.next()) {
      cart.addToCart(new CartItem(rs.getString(3), rs.getInt(5)));
    }
    return cart;
  }

  /**
   * Clears cart identified by uuid of any items that may belong to it. function
   * calls a stored procedure as opposed of executing a delete query due to security
   * reasons.
   * 
   * @param uuid
   *          UUID of a cart which is to be removed
   * @param connection
   * @param cart
   *          cart object currently active for particular uuid. If operation is
   *          successful, this is replaced with clean cart, if not, the same cart is
   *          returned.
   * @return empty cart
   * @throws SQLException
   */
  public static CartObj clearCart(String uuid, CartObj cart, Connection connection)
      throws SQLException {
    String sql = "{CALL clear_cart(?)}";

    try (CallableStatement cs = connection.prepareCall(sql)) {
      cs.setString(1, uuid);
      cs.execute();
      cart = new CartObj();
    }
    return cart;
  }

  /**
   * Purchases particular cart in the database. Cart needs to be identified with uuid
   * as well as user who's purchasing the cart (logged in) needs to be known.
   * Function calls a stored procedure called buy_cart in the database
   * 
   * @param uuid
   *          uuid identifying a cart
   * @param user
   *          logged in user purchasing a cart
   * @param cart
   *          cart object currently active for particular uuid. If operation is
   *          successful, this is replaced with clean cart, if not, the same cart is
   *          returned.
   * @param connection
   * @return
   * @throws SQLException
   */
  public static CartObj buyCart(String uuid, User user, CartObj cart,
      Connection connection) throws SQLException {
    String sql = "{CALL buy_cart(?,?)}";
    try (CallableStatement cs = connection.prepareCall(sql)) {
      cs.setString(1, uuid);
      cs.setInt(2, user.getId());
      cs.execute();
      cart = new CartObj();
    }

    return cart;
  }

}
